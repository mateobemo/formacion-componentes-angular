
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';


// Providers
import { CustomerService } from './services/customer.service';

export const providers = [
  CustomerService
];

@NgModule({
    declarations: [],
    imports: [CommonModule, ReactiveFormsModule],
    providers: [],
    exports: []
  })
  export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [providers]
        };
    }
  }
