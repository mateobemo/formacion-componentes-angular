import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { ModuleWithProviders } from '@angular/core';

export const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: [
      { path: '', redirectTo: 'first-view', pathMatch: 'full'},
      { path: 'first-view', loadChildren: 'app/dashboard/first-view/first-view.module#FirstViewModule' }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
