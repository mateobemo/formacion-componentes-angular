import { Component, ViewEncapsulation } from '@angular/core';
import { CustomerService } from '../../../shared/services/customer.service';
@Component({
  selector: 'app-first-view',
  templateUrl: './first-view.component.html',
  styleUrls: ['./first-view.component.scss'],
  encapsulation: ViewEncapsulation.None, // DataTableStyle
})
export class FirstViewComponent {
  columns = [
    {'name': 'name'},
    {'name': 'address'},
    {'name': 'cityStateZip'}
  ];
  rows = [];
  placeholder = 'Filtra por todos los campos';
  constructor(private service: CustomerService) {
    this.service.getCustomers().subscribe( data => this.rows = data);
  }
  updateTable(event) {
    this.service.getCustomers(event).subscribe( data => this.rows = data);

  }
}
