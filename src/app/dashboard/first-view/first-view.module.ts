import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { routing } from './first-view.routing';

import { FirstViewComponent } from '../first-view/first-view.component';

import { SharedModule } from '../../../shared/shared.module';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule, FormsModule, routing, NgxDatatableModule, SharedModule
  ],
  declarations: [FirstViewComponent]
})
export class FirstViewModule { }
